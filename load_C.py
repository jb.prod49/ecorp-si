import csv
import inflect
from config.bdd_config import DB_CONFIG
from fonction import connect_bdd, normalize_product_name, creer_stock,close_connect

p = inflect.engine()



#-----------------------------------------#
#-------- récupérer les données ----------#
#-----------------------------------------#
tab = {}

with open('ressources/extract_C.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for row in reader:
        columns = row[0].split(',')
        produit = normalize_product_name(columns[0])
        quantite = int(columns[1])

        if produit in tab:
            tab[produit] += quantite
        else:
            tab[produit] = quantite
        if tab[produit]< 0 :
                tab[produit] = 0

final_table = [{"produit": produit, "stock": quantite} for produit, quantite in tab.items()]

# ---------------------------------------------------- #
# -------- injecter les données dans la BDD ---------- #
# ---------------------------------------------------- #
connect = connect_bdd(DB_CONFIG)
nom_restaurant = "restaurant C"

creer_stock(nom_restaurant,final_table,connect)
close_connect(connect)