import csv
import inflect
from config.bdd_config import DB_CONFIG
from fonction import connect_bdd, normalize_product_name, creer_recette,close_connect

p = inflect.engine()
                    
#-----------------------------------------#
#-------- récupérer les données ----------#
#-----------------------------------------#
tab = {}
final_table=[]
with open('ressources/recettes.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    next(reader, None)
    for row in reader:
        columns = row[0].split(',')
        recette = columns[2]
        recette = recette.replace('"', '')
        recette = recette.strip()
        produit = normalize_product_name(columns[0])
        quantite = columns[1]
        tab = {"recette": recette, "produit": produit, "quantite": quantite}
        final_table.append(tab)


# ---------------------------------------------------- #
# -------- injecter les données dans la BDD ---------- #
# ---------------------------------------------------- #
connect = connect_bdd(DB_CONFIG)

creer_recette(final_table, connect)
close_connect(connect)
