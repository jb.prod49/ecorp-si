import subprocess
from fastapi import FastAPI
from fonction_API import router as router
from config.bdd_config import DB_CONFIG

#-------------------------------------------------#
#-------- exécution du fichier init.sql ----------#
#-------------------------------------------------#

sql_file_path = 'init.sql'
command = f'mysql --defaults-extra-file=config/my.cnf {DB_CONFIG["database"]} < {sql_file_path}'

subprocess.run(command, shell=True)

#--------------------------------------------------#
#-------- exécution du fichier load_A.py ----------#
#--------------------------------------------------#
with open('load_A.py', 'r') as file:
    code = file.read()
    exec(code)

#--------------------------------------------------#
#-------- exécution du fichier load_B.py ----------#
#--------------------------------------------------#
with open('load_B.py', 'r') as file:
    code = file.read()
    exec(code)

#--------------------------------------------------#
#-------- exécution du fichier load_C.py ----------#
#--------------------------------------------------#
with open('load_C.py', 'r') as file:
    code = file.read()
    exec(code)
    
#------------------------------------------------------#
#-------- exécution du fichier migration.sql ----------#
#------------------------------------------------------#

sql_file_path = 'migration.sql'
command = f'mysql --defaults-extra-file=config/my.cnf {DB_CONFIG["database"]} < {sql_file_path}'

subprocess.run(command, shell=True)

#--------------------------------------------------------#
#-------- exécution du fichier test_recette.py ----------#
#--------------------------------------------------------#
sql_file_path = 'test_recettes.sql'
command = f'mysql --defaults-extra-file=config/my.cnf {DB_CONFIG["database"]} < {sql_file_path}'

subprocess.run(command, shell=True)
#---------------------------------------------------#
#-------- exécution du fichier recette.py ----------#
#---------------------------------------------------#
with open('load_recette.py', 'r') as file:
    code = file.read()
    exec(code)
    
#-----------------------------------------------------#
#-------- Route pour les fonctions de l'API ----------#
#-----------------------------------------------------#

app = FastAPI(debug=True)
app.include_router(router)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)