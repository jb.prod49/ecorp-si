USE ECORP_SI;

CREATE TABLE IF NOT EXISTS `restaurant` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255)
);

CREATE TABLE IF NOT EXISTS `produit` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255)
);

CREATE TABLE IF NOT EXISTS `stock` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `id_restaurant` int,
  `id_produit` int,
  `qt` int,
  `date` date
);

CREATE TABLE IF NOT EXISTS `recette` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255)
);

CREATE TABLE IF NOT EXISTS `quantite` (
  `id_produit` int,
  `id_recette` int,
  `qt` int,
  PRIMARY KEY (id_produit, id_recette)
);

ALTER TABLE `stock` ADD FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id`);
ALTER TABLE `stock` ADD FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id`);
ALTER TABLE `quantite` ADD FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id`);
ALTER TABLE `quantite` ADD FOREIGN KEY (`id_recette`) REFERENCES `recette` (`id`);
