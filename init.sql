DROP DATABASE IF EXISTS ECORP_SI;
CREATE DATABASE ECORP_SI;
USE ECORP_SI;

CREATE TABLE `restaurant` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255)
);

CREATE TABLE `produit` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255)
);

CREATE TABLE `stock` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `id_restaurant` INT,
  `id_produit` INT,
  `qt` FLOAT,
  `date_ajout` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE `stock` ADD FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id`);
ALTER TABLE `stock` ADD FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id`);