from typing import List
from fastapi import APIRouter
from pydantic import BaseModel
from config.bdd_config import DB_CONFIG
from fonction import connect_bdd, execute_query, normalize_product_name, ajout_produit
from fonction import ajout_recette, ajout_stock, ajout_quantite


router = APIRouter()

# --------------------------------- #
# --------- Connexion BDD --------- #
# --------------------------------- #
connect = connect_bdd(DB_CONFIG)

# ------------------------------- #
# --------- Les classes --------- #
# ------------------------------- #

class Produit(BaseModel):
    id: int | None = None
    nom: str

class Restaurant(BaseModel):
    id: int | None = None
    nom: str | None = None

class Ingredient(BaseModel):
    nom_produit: str
    quantite: int

class Recette(BaseModel):
    nom: str
    ingredients: List[Ingredient]

class Stock(BaseModel):
    id_produit: int | None = None
    id_restaurant: int | None = None
    qt: float | None = None
    
# ------------------------------------- #
# -------- creation des routes ---------#
# ------------------------------------- #

#En envoyant le nom d’un produit, connaître l’état des stocks dans
#chaque restaurant ainsi que le stock global (cumulé).
@router.get("/produit/{nom_produit}")
def stock_produit(nom_produit: str):
    connect = connect_bdd(DB_CONFIG)
    nom_produit = normalize_product_name(nom_produit)

    query_select = """SELECT 
                        p.nom AS nom_produit,
                        r.nom AS nom_restaurant,
                        s.qt AS quantité
                    FROM 
                        stock s
                    JOIN 
                        produit p ON s.id_produit = p.id
                    JOIN
                        restaurant r ON s.id_restaurant = r.id
                    JOIN
                       (SELECT 
                            s.id_produit, 
                            s.id_restaurant, 
                            MAX(s.date_ajout) AS derniere_date
                        FROM 
                            stock s
                        JOIN 
                            produit p ON s.id_produit = p.id
                        JOIN
                            restaurant r ON s.id_restaurant = r.id
                        WHERE 
                            p.nom = %s
                        GROUP BY
                            s.id_produit, s.id_restaurant) AS dernier_ajout
                    ON 
                        s.id_produit = dernier_ajout.id_produit AND
                        s.id_restaurant = dernier_ajout.id_restaurant AND
                        s.date_ajout = dernier_ajout.derniere_date
                    WHERE 
                        p.nom = %s;"""

    result = execute_query(query_select, (nom_produit, nom_produit), connect)
    somme_stock = sum(item[2] for item in result) if result else 0
    somme_stock = "stock total: ", somme_stock
    return result, somme_stock

# Pouvoir ajouter un nouveau produit.
@router.post("/produit/")
def new_produit(produit: Produit):
    connect = connect_bdd(DB_CONFIG)
    ajout_produit(produit.nom, connect)
    return produit

# Pouvoir augmenter ou diminuer le stock d’un produit dans un
# restaurant donné.
@router.put("/produit/")
def modifier_stock(stock: Stock):
    connect = connect_bdd(DB_CONFIG)
    query_select = """ SELECT 
                            qt AS quantite
                        FROM 
                            stock
                        WHERE
                            id_produit = %s 
                            AND
                            id_restaurant = %s
                        ORDER BY
                            date_ajout DESC
                        LIMIT 1;
    """
    result = execute_query(query_select, (stock.id_produit, stock.id_restaurant), connect)
    qt_base = result[0][0]
    new_qt = qt_base + stock.qt
    if new_qt < 0:
        new_qt = 0 
    ajout_stock(stock.id_restaurant,stock.id_produit,new_qt,connect)
    return stock

# Ajouter une recette (la liste de ses ingrédients et les quantités).
@router.post("/recette/")
def new_recette(recette: Recette):
    connect = connect_bdd(DB_CONFIG)
    id_recette = ajout_recette(recette.nom, connect)
    
    for ingredient in recette.ingredients:
        id_produit = ajout_produit(ingredient.nom_produit, connect)
        ajout_quantite(id_recette, id_produit, ingredient.quantite, connect)
    
    return {"message": "Recette et ingrédients ajoutés avec succès"}

# Pouvoir retrouver une recette grâce à son nom.
@router.get("/recette/{nom_recette}")
def recherche_recette(nom_recette):
    recette = []
    connect = connect_bdd(DB_CONFIG)
    query_select = """ SELECT
                            r.id, 
                            r.nom AS nom_recette
                        FROM
                            recette r
                        WHERE
                            r.nom = %s;
    """
    result = execute_query(query_select, (nom_recette,), connect)
    id_recette = result[0][0]
    nom_recette = result[0][1]
    recette.append(nom_recette)
    connect = connect_bdd(DB_CONFIG)
    query_select = """ SELECT
                            p.nom AS 'nom produit',
                            q.qt
                        FROM
                            quantite q
                        JOIN
                            produit p ON q.id_produit = p.id
                        WHERE
                            q.id_recette = %s;
    """
    result = execute_query(query_select, (id_recette,), connect)
    ingredient = {}
    for row in result: 
        ingredient = {'ingredient': row[0], 'quantite': row[1]}
        recette.append(ingredient)
    return recette

# Pouvoir savoir combien de fois je peux faire une recette donnée
# dans chacun des restaurants.
# @router.get("/nb_recette/{nom_recette}")
def calculer_nombre_recettes(nom_recette):
    connect = connect_bdd(DB_CONFIG)
    nom_recette = nom_recette.strip()
    query_ingredients = """ SELECT 
                                q.id_produit, 
                                q.qt
                            FROM 
                                quantite q
                            JOIN 
                                recette r ON q.id_recette = r.id
                            WHERE 
                                r.nom = %s;"""
    ingredients = execute_query(query_ingredients, (nom_recette,), connect)

    resultats = {}
    for id_produit, qt_necessaire in ingredients:
        query_stocks = """ SELECT 
                                s.id_restaurant, 
                                MAX(s.qt) AS qt
                            FROM 
                                stock s
                            INNER JOIN (
                                SELECT 
                                    id_produit, 
                                    id_restaurant, 
                                    MAX(date_ajout) AS derniere_date
                                FROM 
                                    stock
                                WHERE 
                                    id_produit = %s
                                GROUP BY 
                                    id_produit, id_restaurant
                            ) AS derniers_stocks
                            ON s.id_produit = derniers_stocks.id_produit 
                            AND s.id_restaurant = derniers_stocks.id_restaurant 
                            AND s.date_ajout = derniers_stocks.derniere_date
                            WHERE 
                                s.id_produit = %s
                            GROUP BY 
                                s.id_restaurant;"""
        stocks = execute_query(query_stocks, (id_produit, id_produit), connect)
        for id_restaurant, qt_disponible in stocks:
            nb_possible = qt_disponible // qt_necessaire
            resultats[id_restaurant] = nb_possible
            if resultats[id_restaurant] > nb_possible:
                resultats[id_restaurant] = nb_possible

    return resultats

print(calculer_nombre_recettes('Tourte lagon'))