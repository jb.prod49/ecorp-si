import csv
import inflect
from datetime import datetime
from config.bdd_config import DB_CONFIG
from fonction import connect_bdd, normalize_product_name, creer_stock,close_connect

p = inflect.engine()

#-----------------------------------------#
#-------- récupérer les données ----------#
#-----------------------------------------#
# tab = {}
# final_tab =[]
# with open('ressources/extract_B.csv', newline='') as csvfile:
#     reader = csv.DictReader(csvfile)
#     for row in reader:
#         product = normalize_product_name(row['stock'])
#         stock = int(row['inventory'])
#         date_str = row['date']
#         date_obj = datetime.strptime(date_str, '%d/%m/%Y').date()
        
#         # print("produit : ",product,"stock : ", stock, "date : ", date_obj)
#         tab = {"produit": product, "stock": stock, "date": date_obj}
#         final_tab.append(tab)

tab = {}

with open('ressources/extract_B.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    next(reader, None)
    for row in reader:
        columns = row[0].split(',')
        produit = normalize_product_name(columns[0])
        date_str = columns[1]
        date_obj = datetime.strptime(date_str, '%d/%m/%Y').date()
        stock = int(columns[2])
        
        key = (produit, date_obj)
        if key in tab:
            tab[key] += stock
        else:
            tab[key] = stock

final_table = [{"produit": produit, "date": date, "stock": stock} for (produit, date), stock in tab.items()]
final_table = sorted(final_table, key=lambda x: x['date'])

# ---------------------------------------------------- #
# -------- injecter les données dans la BDD ---------- #
# ---------------------------------------------------- #
connect = connect_bdd(DB_CONFIG)
nom_restaurant = "restaurant B"
creer_stock(nom_restaurant,final_table,connect)
close_connect(connect)
