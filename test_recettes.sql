-- Insérez des recettes 
INSERT INTO recette (nom) VALUES ('Gâteau Magique');
INSERT INTO recette (nom) VALUES ('Potion Mystique');

-- Insérez les produits 
INSERT INTO produit (nom) VALUES ('Oeufs');
INSERT INTO produit (nom) VALUES ('farine');
INSERT INTO produit (nom) VALUES ('sucre');
INSERT INTO produit (nom) VALUES ('Larmes de licorne');
INSERT INTO produit (nom) VALUES ("Poussière d'étoile");

-- Récupérez les ID de produits et de recettes
SET @gateau_magique = (SELECT id FROM recette WHERE nom = 'Gâteau Magique' LIMIT 1);
SET @potion_mystique = (SELECT id FROM recette WHERE nom = 'Potion Mystique' LIMIT 1);

SET @oeufs = (SELECT id FROM produit WHERE nom = 'Oeufs' LIMIT 1);
SET @farine = (SELECT id FROM produit WHERE nom = 'Farine' LIMIT 1);
SET @sucre = (SELECT id FROM produit WHERE nom = 'Sucre' LIMIT 1);
SET @larmes_licorne = (SELECT id FROM produit WHERE nom = 'Larmes de licorne' LIMIT 1);
SET @poussiere_etoile = (SELECT id FROM produit WHERE nom = "Poussière d\'étoile" LIMIT 1);

-- Insérez les quantités nécessaires pour le Gâteau Magique
INSERT INTO quantite (id_produit, id_recette, qt) VALUES (@oeufs, @gateau_magique, 3);
INSERT INTO quantite (id_produit, id_recette, qt) VALUES (@farine, @gateau_magique, 200);
INSERT INTO quantite (id_produit, id_recette, qt) VALUES (@larmes_licorne, @gateau_magique, 5);

-- Insérez les quantités nécessaires pour la Potion Mystique
INSERT INTO quantite (id_produit, id_recette, qt) VALUES (@sucre, @potion_mystique, 100);
INSERT INTO quantite (id_produit, id_recette, qt) VALUES (@poussiere_etoile, @potion_mystique, 10);
