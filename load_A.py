# Importe les modules nécessaires :
import csv
# csv pour la lecture de fichiers CSV, 
import inflect
# inflect pour convertir les nombres en mots, 

from config.bdd_config import DB_CONFIG
# DB_CONFIG pour la configuration de la base de données, 

from fonction import connect_bdd, normalize_product_name, creer_stock, close_connect
# et des fonctions pour la connexion à la base de données, la normalisation des noms de produits, 
# la création du stock et la fermeture de la connexion à la base de données.

p = inflect.engine()
# Crée une instance de l'engine inflect pour convertir les nombres en mots.

#-----------------------------------------#
#-------- récupérer les données ----------#
#-----------------------------------------#
tab = {}
# Initialise un dictionnaire vide pour stocker les données extraites.

with open('ressources/extract_A.csv', newline='') as csvfile:
    # Ouvre le fichier CSV 'extract_A.csv' en mode lecture.
    reader = csv.reader(csvfile, delimiter='\t')
    # Crée un objet reader pour lire le fichier CSV, en utilisant le tab comme séparateur de colonnes.
    next(reader, None)
    # Saute la première ligne (en-tête) du fichier CSV.
    for row in reader:
        # Itère sur chaque ligne du fichier CSV.
        columns = row[0].split(',')
        # Sépare la ligne en colonnes en utilisant la virgule comme séparateur.
        produit = normalize_product_name(columns[0])
        # Normalise le nom du produit en utilisant la fonction normalize_product_name.
        stock = int(columns[1])
        # Convertit la deuxième colonne (stock) en entier.
        if produit in tab:
            # Vérifie si le produit est déjà dans le dictionnaire.
            tab[produit] += stock
            # Si oui, ajoute le stock à la valeur existante.
        else:
            tab[produit] = stock
            # Si non, crée une nouvelle entrée dans le dictionnaire avec le stock.
final_table = [{"produit": produit, "stock": stock} for produit, stock in tab.items()]
# Crée une liste de dictionnaires avec les produits et les stocks pour faciliter l'insertion dans la base de données.

# ---------------------------------------------------- #
# -------- injecter les données dans la BDD ---------- #
# ---------------------------------------------------- #
connect = connect_bdd(DB_CONFIG)
# Établit une connexion à la base de données en utilisant la configuration DB_CONFIG.
nom_restaurant = "restaurant A"
# Définit le nom du restaurant.

creer_stock(nom_restaurant, final_table, connect)
# Appelle la fonction creer_stock pour créer les entrées de stock dans la base de données.
close_connect(connect)
# Ferme la connexion à la base de données.
