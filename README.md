# ECORP SI - Gestion des Recettes et Stocks pour Restaurants

## Description

Ce projet FastAPI est conçu pour la gestion des stocks et des recettes
dans le cadre de restaurants. Il offre une gamme de fonctionnalités, y
compris la consultation des stocks, la modification des stocks, l'ajout
de nouvelles recettes avec des ingrédients, et la recherche de recettes.
 Une fonctionnalité supplémentaire permet de déterminer le nombre de
fois qu'une recette donnée peut être préparée dans chaque restaurant en
fonction des stocks disponibles.

## Fonctionnalités

* **Consultation des Stocks** : Vérifier l'état des stocks pour un produit spécifique dans chaque restaurant.
* **Ajout de Produits** : Intégrer de nouveaux produits à la base de données.
* **Modification des Stocks** : Ajuster le stock d'un produit dans un restaurant donné.
* **Gestion des Recettes** : Ajouter des recettes avec une liste d'ingrédients et leurs quantités.
* **Recherche de Recettes** : Trouver des recettes par leur nom.
* **Calcul des Possibilités de Recettes** : Évaluer combien de fois une recette peut être préparée dans chaque restaurant en fonction des stocks disponibles.

## Installation

### Prérequis

* Python 3.8 ou plus récent.
* MySQL installé et configuré.

### Étapes d'installation

1. **Clonage du dépôt** :

   ```bash
   git clone https://gitlab.com/jb.prod49/ecorp-si.git
   cd ecorp-si
   ```
2. **Installation des dépendances** :

   ```bash
   pip install -r requirements.txt
   ```
3. **Configuration de la base de données** :

   - Configurez vos informations de connexion dans config/bdd_config.py.
   - Exécutez les scripts SQL pour initialiser la base de données.
4. **Démarrage de l'application** :

   ```bash
   python main.py
   ```

## Utilisation de l'API

Après le démarrage de l'application, elle sera accessible via `http://127.0.0.1:8000`. Les routes disponibles sont :

* `GET /produit/{nom_produit}` : Obtenir l'état des stocks d'un produit spécifique.
* `POST /produit/` : Ajouter un nouveau produit.
* `PUT /produit/` : Modifier le stock d'un produit dans un restaurant.
* `POST /recette/` : Ajouter une nouvelle recette et ses ingrédients.
* `GET /recette/{nom_recette}` : Rechercher une recette par son nom.
* `POST /nb_recette/{nom_recette}` : Calculer le nombre de fois qu'une recette peut être préparée dans chaque restaurant.

## Technologies Utilisées

* FastAPI
* MySQL
* Python

## Contribution

Les contributions à ce projet sont les bienvenues. Veuillez forker le projet, apporter vos modifications et soumettre une pull request pour examen.

## Licence

Ce projet est distribué sous la licence basique.

## Contact

Pour toute question ou suggestion, n'hésitez pas à contacter.
