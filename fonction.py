import csv
import inflect
import unicodedata
import mysql.connector
from mysql.connector import Error
from config.bdd_config import DB_CONFIG

p = inflect.engine()

#----------------------------------------------------------#
#-------- Fonction pour exécuter une requête SQL ----------#
#----------------------------------------------------------#
def connect_bdd(db_config):
    connect = mysql.connector.connect(**db_config)
    return connect

def execute_query(query: str, params=None, connect=None, fetch=True):
    try:
        cursor = connect.cursor()
        # print(f"Executing query: {query} with params: {params}")
        cursor.execute(query, params)
        result = cursor.fetchall() if fetch else None
        connect.commit()
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        return None


def close_connect(connect):
    if connect.is_connected():
        connect.close()

#---------------------------------------------#
#-------- Définitions des fonctions ----------#
#---------------------------------------------#

def ajout_restaurant(connect, nom: str) -> int:
    query_select = "SELECT id FROM restaurant WHERE nom = %s"
    result = execute_query(query_select, (nom,), connect)

    if result:
        restaurant_id = result[0][0]
        # print(f"Le restaurant '{nom}' existe déjà. Son ID est : {restaurant_id}")
    else:
        query_insert = "INSERT INTO restaurant (nom) VALUES (%s)"
        execute_query(query_insert, (nom,), connect, fetch=False)
        
        query_get_id = "SELECT id FROM restaurant WHERE nom = %s"
        new_result = execute_query(query_get_id, (nom,), connect)
        if new_result:
            restaurant_id = new_result[0][0]
            # print(f"Le restaurant '{nom}' a été ajouté avec l'ID : {new_restaurant_id}")
        else:
            print(f"Impossible de récupérer l'ID du restaurant '{nom}'.")
    return restaurant_id, connect
            
def ajout_produit(nom_produit, connect):
    query_select = "SELECT id FROM produit WHERE nom = %s"
    result = execute_query(query_select, (nom_produit,), connect)

    if result:
        product_id = result[0][0]
        # print(f"Le produit '{nom_produit}' existe déjà. Son ID est : {product_id}")
    else:
        query_insert_product = "INSERT INTO produit (nom) VALUES (%s)"
        execute_query(query_insert_product, (nom_produit,), connect, fetch=False)
        
        query_select = "SELECT id FROM produit WHERE nom = %s"
        product_id_result = execute_query(query_select, (nom_produit,), connect)

        if product_id_result:
            product_id = product_id_result[0][0]
            # print(f"ID du dernier produit ajouté : {product_id}")
        else:
            print(f"Impossible de récupérer l'ID du produit '{nom_produit}'.")    
    return product_id

def ajout_stock(restaurant_id, product_id, quantity,connect,date = None):
    if date :
        query_insert_stock = "INSERT INTO stock (id_restaurant, id_produit, qt, date_ajout) VALUES (%s, %s, %s, %s)"
        execute_query(query_insert_stock, (restaurant_id, product_id, quantity, date), connect, fetch=False)
    else:
        query_insert_stock = "INSERT INTO stock (id_restaurant, id_produit, qt) VALUES (%s, %s, %s)"
        execute_query(query_insert_stock, (restaurant_id, product_id, quantity), connect, fetch=False)
    # print(f"Les informations ont été ajoutées dans la table stock.")

def ajout_quantite(recette_id, product_id, quantity,connect):
    query_insert_stock = "INSERT INTO quantite (id_recette, id_produit, qt) VALUES (%s, %s, %s)"
    execute_query(query_insert_stock, (recette_id, product_id, quantity), connect, fetch=False)
    # print(f"Les informations ont été ajoutées dans la table quantite.")

def ajout_recette(nom_recette,connect):
    query_select = "SELECT id FROM recette WHERE nom = %s"
    result = execute_query(query_select, (nom_recette,), connect)
    if result:
        recette_id = result[0][0]
        query_select = "DELETE FROM quantite WHERE id_recette = %s"
        result = execute_query(query_select, (recette_id,), connect)
        
        query_select = "DELETE FROM recette WHERE id = %s"
        result = execute_query(query_select, (recette_id,), connect)
       
    query_insert = "INSERT INTO recette (nom) VALUES (%s)"
    execute_query(query_insert, (nom_recette,), connect, fetch=False)
    
    query_get_id = "SELECT id FROM recette WHERE nom = %s"
    new_result = execute_query(query_get_id, (nom_recette,), connect)
    if new_result:
        recette_id = new_result[0][0]
        # print(f"La recette '{nom_recette}' a été ajouté avec l'ID : {new_recette_id}")
    else:
        print(f"Impossible de récupérer l'ID de la recette '{nom_recette}'.")
    
    return recette_id

def creer_recette(tab_recette, connect):
    for row in tab_recette:
        id_recette = ajout_recette(row['recette'],connect)
        id_produit = ajout_produit(row['produit'],connect)
        ajout_quantite(id_recette,id_produit,row['quantite'],connect)

def creer_stock(nom_restaurant, tab_produits, connect):
    id_restaurant, connect = ajout_restaurant(connect, nom_restaurant)
    for row in tab_produits:
        id_produit = ajout_produit(row['produit'], connect)
        if 'date' in row and row['date']:
            date = row['date']
            ajout_stock(id_restaurant, id_produit, row['stock'], connect, date)
        else:
            ajout_stock(id_restaurant, id_produit, row['stock'], connect)

def normalize_product_name(product_name):
    product_name = product_name.strip()
    product_name = product_name.lower()
    product_name = ''.join(char for char in unicodedata.normalize('NFKD', product_name) if unicodedata.category(char) != 'Mn')
    product_name = product_name.replace("œ", "oe")
    product_name = p.singular_noun(product_name) or product_name
    return product_name

def close_connection(connect):
    connect.close()